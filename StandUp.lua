local playerGUID
local DoEmote = _G.DoEmote
local CombatLogGetCurrentEventInfo = _G.CombatLogGetCurrentEventInfo
local f = CreateFrame("Frame")
f:RegisterEvent("PLAYER_LOGIN")
f:SetScript("OnEvent", function(self)
	playerGUID = UnitGUID("player")
	self:UnregisterEvent("PLAYER_LOGIN")
	self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	self:SetScript("OnEvent", function()
		local _, event, _, _, _, _, _, destGUID = CombatLogGetCurrentEventInfo()
		if destGUID == playerGUID and (event == "SWING_DAMAGE" or event == "SPELL_DAMAGE") then
			DoEmote("STAND")
		end
	end)
end)
